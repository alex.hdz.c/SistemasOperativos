CPU0       CPU1       CPU2       CPU3
0:         17          0          0          0  IR-IO-APIC   2-edge      timer
1:       4851         63        627         50  IR-IO-APIC   1-edge      i8042
8:          1          0          0          0  IR-IO-APIC   8-edge      rtc0
9:        973         49        467        103  IR-IO-APIC   9-fasteoi   acpi
12:     483212       7370      70929       6544  IR-IO-APIC  12-edge      i8042
16:        294         12        178         19  IR-IO-APIC  16-fasteoi   ehci_hcd:usb1
17:       1429       2981     108612      14541  IR-IO-APIC  17-fasteoi   wlp2s0, mmc0
21:         86          5         64          1  IR-IO-APIC  21-fasteoi   ehci_hcd:usb2
23:          0          0          0          0  IR-IO-APIC  23-edge      smo8800
24:          0          0          0          0  DMAR-MSI   0-edge      dmar0
25:          0          0          0          0  DMAR-MSI   1-edge      dmar1
26:          0          0          0          0  IR-PCI-MSI 327680-edge      xhci_hcd
27:      20645        402      27186        810  IR-PCI-MSI 512000-edge      ahci[0000:00:1f.2]
28:          3          0         20          0  IR-PCI-MSI 360448-edge      mei_me
29:      71164          4         79      56286  IR-PCI-MSI 32768-edge      i915
30:          0         20         70         91  IR-PCI-MSI 442368-edge      snd_hda_intel:card0
31:          2      28594          5          0  IR-PCI-MSI 409600-edge      eno1
NMI:          7         18         21         19   Non-maskable interrupts
LOC:     348721     326952     352971     325697   Local timer interrupts
SPU:          0          0          0          0   Spurious interrupts
PMI:          7         18         21         19   Performance monitoring interrupts
IWI:          0          0          3          0   IRQ work interrupts
RTR:          0          0          0          0   APIC ICR read retries
RES:      58017      54390      47967      45426   Rescheduling interrupts
CAL:      61960      71467      61859      60275   Function call interrupts
TLB:      58288      68583      59566      57311   TLB shootdowns
TRM:          0          0          0          0   Thermal event interrupts
THR:          0          0          0          0   Threshold APIC interrupts
DFR:          0          0          0          0   Deferred Error APIC interrupts
MCE:          0          0          0          0   Machine check exceptions
MCP:         16         15         15         15   Machine check polls
ERR:          0
MIS:          0
PIN:          0          0          0          0   Posted-interrupt notification event
PIW:          0          0          0          0   Posted-interrupt wakeup event
