ruta H/W         Dispositivo  Clase          Descripción
=========================================================
                              system         Latitude E6430 (Latitude E6430)
/0                            bus            0CPWYR
/0/0                          memory         64KiB BIOS
/0/54                         processor      Intel(R) Core(TM) i5-3320M CPU @ 2.60GHz
/0/54/44                      memory         512KiB L2 caché
/0/54/45                      memory         128KiB L1 caché
/0/54/46                      memory         3MiB L3 caché
/0/47                         memory         6GiB Memoria de sistema
/0/47/0                       memory         4GiB SODIMM DDR3 Síncrono 1600 MHz (0.6 ns)
/0/47/1                       memory         2GiB SODIMM DDR3 Síncrono 1600 MHz (0.6 ns)
/0/100                        bridge         3rd Gen Core processor DRAM Controller
/0/100/2                      display        3rd Gen Core processor Graphics Controller
/0/100/14                     bus            7 Series/C210 Series Chipset Family USB xHCI Host Controller
/0/100/14/0      usb3         bus            xHCI Host Controller
/0/100/14/1      usb4         bus            xHCI Host Controller
/0/100/16                     communication  7 Series/C216 Chipset Family MEI Controller #1
/0/100/19        eno1         network        82579LM Gigabit Network Connection
/0/100/1a                     bus            7 Series/C216 Chipset Family USB Enhanced Host Controller #2
/0/100/1a/1      usb1         bus            EHCI Host Controller
/0/100/1a/1/1                 bus            Integrated Rate Matching Hub
/0/100/1a/1/1/5               multimedia     Laptop_Integrated_Webcam_E4HD
/0/100/1b                     multimedia     7 Series/C216 Chipset Family High Definition Audio Controller
/0/100/1c                     bridge         7 Series/C216 Chipset Family PCI Express Root Port 1
/0/100/1c.1                   bridge         7 Series/C210 Series Chipset Family PCI Express Root Port 2
/0/100/1c.1/0    wlp2s0       network        BCM4313 802.11bgn Wireless Network Adapter
/0/100/1c.2                   bridge         7 Series/C210 Series Chipset Family PCI Express Root Port 3
/0/100/1c.3                   bridge         7 Series/C216 Chipset Family PCI Express Root Port 4
/0/100/1c.5                   bridge         7 Series/C210 Series Chipset Family PCI Express Root Port 6
/0/100/1c.5/0                 generic        OZ600FJ0/OZ900FJ0/OZ600FJS SD/MMC Card Reader Controller
/0/100/1d                     bus            7 Series/C216 Chipset Family USB Enhanced Host Controller #1
/0/100/1d/1      usb2         bus            EHCI Host Controller
/0/100/1d/1/1                 bus            Integrated Rate Matching Hub
/0/100/1d/1/1/8               generic        5880
/0/100/1f                     bridge         QM77 Express Chipset LPC Controller
/0/100/1f.2                   storage        82801 Mobile SATA Controller [RAID mode]
/0/100/1f.3                   bus            7 Series/C216 Chipset Family SMBus Controller
/0/1             scsi0        storage        
/0/1/0.0.0       /dev/sda     disk           500GB TOSHIBA MQ01ABD0
/0/1/0.0.0/1     /dev/sda1    volume         100MiB Windows NTFS volumen
/0/1/0.0.0/2     /dev/sda2    volume         198GiB Windows NTFS volumen
/0/1/0.0.0/3     /dev/sda3    volume         143GiB Extended partition
/0/1/0.0.0/3/5   /dev/sda5    volume         143GiB partición EXT4
/0/1/0.0.0/4     /dev/sda4    volume         124GiB Windows NTFS volumen
/0/2             scsi1        storage        
/0/2/0.0.0       /dev/cdrom   disk           DVD+-RW DU-8A5HH
/1                            power          DELL JYPJ139
/2                            power          To Be Filled By O.E.M.
/3               docker0      network        Ethernet interface
